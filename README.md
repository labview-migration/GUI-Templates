# README
This project contains some templates for Python GUI Applications.

- **ttk_gui_app_template.py**<br/>This template is meant as starting point for desktop apps using modern themed [TKinter](https://docs.python.org/3/library/tkinter.html).
- **dpg_gui_app_template.py**<br/>This template is meant as starting point for desktop apps using [DearPyGui](https://pypi.org/project/dearpygui/).
- **qt_gui_app_template.py**<br/>This template is meant as starting point for desktop apps using [PyQt5](https://pypi.org/project/PyQt5/).

Lizenziert unter EUPL V. 1.2

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
