#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
<template> is inspired by Moor Moore, Alan D.

Mastering GUI Programming with Python:
Develop impressive cross-platform GUI applications with PyQt.
Packt Publishing. Kindle-Version.

Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

Dieses Programm ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiter verteilen und/oder modifizieren.

Dieses Program wird in der Hoffnung bereitgestellt, dass es nützlich sein wird, jedoch
OHNE JEDE GEWÄHR,; sogar ohne die implizite
Gewähr der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
Siehe die GNU General Public License für weitere Einzelheiten.

Sie sollten eine Kopie der GNU General Public License zusammen mit diesem
Program erhalten haben. Wenn nicht, siehe <https://www.gnu.org/licenses/>.
"""

import sys
from PyQt5 import QtWidgets as qtw
# from PyQt5 import QtGui as qtg
from PyQt5 import QtCore as qtc


class SettingsDialog(qtw.QDialog):
    """Dialog for setting the settings."""

    def __init__(self, settings, parent=None):
        super().__init__(parent, modal=True)
        self.setLayout(qtw.QFormLayout())
        self.settings = settings
        self.layout().addRow(qtw.QLabel('<h1>Application Settings</h1>'),)
        self.show_warnings_cb = qtw.QCheckBox(checked=settings.value('show_warnings', type=bool))
        self.layout().addRow("Show Warnings", self.show_warnings_cb)
        self.accept_btn = qtw.QPushButton('Ok', clicked=self.accept)
        self.cancel_btn = qtw.QPushButton('Cancel', clicked=self.reject)
        self.layout().addRow(self.accept_btn, self.cancel_btn)

    def accept(self):
        """Accept warnings."""
        self.settings.setValue('show_warnings', self.show_warnings_cb.isChecked())
        print(self.settings.value('show_warnings'))
        super().accept()
# End of SettingsDialog class


class MainWindow(qtw.QMainWindow):
    """Main Application Window class."""

    settings = qtc.QSettings('<template>', '<template>')  # Load settings

    def __init__(self):
        """Initialize main window."""
        super().__init__()
        # Main UI code goes here
        #
        # The Central Widget: Replace with application specific widget!
        self.central_widget = qtw.QLabel('Replace with <template> specific widget!')
        self.setCentralWidget(self.central_widget)
        # The Statusbar
        self.statusBar().showMessage('Welcome to <template>')
        # The Menubar: Extend/modify as required
        menubar = self.menuBar()
        file_menu = menubar.addMenu('File')
        help_menu = menubar.addMenu('Help')
        # Add File actions: Extend/modify as required
        file_menu.addAction('Settings…', self.show_settings)
        file_menu.addSeparator()  # add separator
        quit_action = file_menu.addAction('Quit', self.close)
        # Add Help actions: Extend/modify as required
        help_menu.addAction('Critical Msg', self.showCriticalDialog)
        help_menu.addAction('Information Msg', self.showInformationDialog)
        help_menu.addAction('Warning Msg', self.showWarningDialog)
        help_menu.addAction('Question Msg', self.showQuestionDialog)
        help_menu.addSeparator()
        help_menu.addAction('About Me', self.showAboutDialog)
        help_menu.addAction('About Qt', self.showAboutQtDialog)

        """
        Show some dialogs after launch
        """
        self.showBetaHintDialog()
        if self.settings.value('show_warnings', False, type=bool):
            self.splash_screen()

        # End main UI code
        self.show()
        # End of __init__

    def show_settings(self):
        """Show setting dialog."""
        settings_dialog = SettingsDialog(self.settings, self)
        settings_dialog.exec()

    def showBetaHintDialog(self):
        """Show QMessageBox Question."""
        response = qtw.QMessageBox.question(self, '<template>', 'This is beta software, do you want to continue?', qtw.QMessageBox.Yes | qtw.QMessageBox.Abort)
        if response == qtw.QMessageBox.Abort:
            self.close()
            sys.exit()

    def showCriticalDialog(self):
        """Show QMessageBox Critical."""
        qtw.QMessageBox.critical(self, "Critical Message", "This is a critical message.")

    def showInformationDialog(self):
        """Show QMessageBox Information."""
        qtw.QMessageBox.information(self, "Information Message", "This is a information message.")

    def showQuestionDialog(self):
        """Show QMessageBox Question."""
        response = qtw.QMessageBox.question(self, '<template>', 'This is beta software, do you want to continue?')
        # Check response with: if response == qtw.QMessageBox.No: # or qtw.QMessageBox.Yes
        return response

    def showWarningDialog(self):
        """Show QMessageBox Warning."""
        qtw.QMessageBox.warning(self, "Warning Message", "This is a warning message.")

    def showAboutDialog(self):
        """Show QMessageBox About."""
        qtw.QMessageBox.about(self, "About <template>.py", "This is a brief <template> description.")

    def showAboutQtDialog(self):
        """Show QMessageBox About Qt."""
        qtw.QMessageBox.aboutQt(self)

    def splash_screen(self):
        """Show QMessageBox Splash-Screen."""
        splash_screen = qtw.QMessageBox()
        splash_screen.setWindowTitle('<template>')
        splash_screen.setText('Replace with short <template> description.')
        splash_screen.setInformativeText('This program comes with\nABSOLUTELY NO WARRANTY!\nDo you really sure you want to use it?')
        splash_screen.setDetailedText(
            'This <template> could used to...\n'
            '\n'
            'Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH\n\n'
            'Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany\n\n'
            'eMail: H.Brand@gsi.de\n\n'
            'Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme\n\n'
            'This program comes with ABSOLUTELY NO WARRANTY.\n\n'
            'This is free software, and you are welcome to'
            'redistribute it under certain conditions.'
        )
        splash_screen.setWindowModality(qtc.Qt.WindowModal)
        splash_screen.addButton(qtw.QMessageBox.Yes)
        splash_screen.addButton(qtw.QMessageBox.Abort)
        response = splash_screen.exec()
        if response == qtw.QMessageBox.Abort:
            self.close()
            sys.exit()
# End od Main Window class


if __name__ == '__main__':
    print("""<template>
Copyright 2024 GSI Helmholtzzentrum für Schwerionenforschung GmbH
Dr. Holger Brand, EEL, Planckstraße 1, 64291 Darmstadt, Germany
eMail: H.Brand@gsi.de
Web: https://www.gsi.de/work/forschung/experimentelektronik/kontrollsysteme
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to
redistribute it under certain conditions.""")
    app = qtw.QApplication(sys.argv)
    # It's required to save a reference to MainWindow.
    # If it goes out of scope, it will be destroyed.
    mw = MainWindow()
    sys.exit(app.exec())
